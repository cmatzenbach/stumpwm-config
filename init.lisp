(in-package :stumpwm)

;; Start swank
(require :swank)
(swank-loader:init)
(swank:create-server :port 4005
                     :style swank:*communication-style*
                     :dont-close t)

;;;; Basics

;; Set primary trigger key
;; (set-prefix-key (kbd "C-,"))
(run-shell-command "xmodmap -e 'clear mod4'" t)
(run-shell-command "xmodmap -e \'keycode 133 = F20\'" t)
(set-prefix-key (kbd "F20"))
;; settings i tried in .Xresources that didn't work
;; clear mod4
;; add mod4 = Super_L

(setf *startup-message* NIL
      *suppress-abort-messages* t
      *shell-program* (getenv "SHELL"))

(setf *message-window-gravity* :center
      *input-window-gravity* :center
      *window-border-style* :thin
      *message-window-padding* 10
      *maxsize-border-width* 5
      *normal-border-width* 5
      *transient-border-width* 2
      stumpwm::*float-window-border* 2
      stumpwm::*float-window-title-height* 5
      *mouse-focus-policy* :click)

(set-normal-gravity :center)
(set-maxsize-gravity :center)
(set-transient-gravity :center)

(set-fg-color "#eeeeee")
(set-bg-color "#1C2028")
(set-border-color "#232731")
(set-focus-color "#3B4252")
(set-unfocus-color "#232731")
(set-win-bg-color "#22272F")
(set-float-focus-color "#3B4252")
(set-float-unfocus-color "#232731")

(setf *colors* (list "#1C2028"      ; 0 black
                     "#BF616A"      ; 1 red
                     "#A3BE8C"      ; 2 green
                     "#EBCB8B"      ; 3 yellow
                     "#5E81AC"      ; 4 blue
                     "#B48EAD"      ; 5 magenta
                     "#8FBCBB"      ; 6 cyan
                     "#ECEFF4"))    ; 7 white

(setf stumpwm:*screen-mode-line-format*
      (list "%w | "
            '(:eval (stumpwm:run-shell-command "date" t))))


;;;; Init

(update-color-map (current-screen))
;; (run-shell-command "sh ~/.screenlayout/multi.sh")
;; (run-shell-command "xmodmap ~/.Xmodmap")
(run-shell-command "feh --bg-scale '/home/matzy/Pictures/DesktopWallpapers/spike-cigarette.jpg'")
;; (run-shell-command "xset b off")


;;;; Keymaps

;; window focusing
(define-key *root-map* (kbd "H") "move-focus left")
(define-key *root-map* (kbd "J") "move-focus down")
(define-key *root-map* (kbd "K") "move-focus up")
(define-key *root-map* (kbd "L") "move-focus right")
;; window placement swapping
(define-key *root-map* (kbd "C-H") "exchange-direction left")
(define-key *root-map* (kbd "C-J") "exchange-direction down")
(define-key *root-map* (kbd "C-K") "exchange-direction up")
(define-key *root-map* (kbd "C-L") "exchange-direction right")
;; move windows (without changing focus)
(define-key *root-map* (kbd "M-H") "exchange-direction right")
(define-key *root-map* (kbd "M-J") "exchange-direction right")
(define-key *root-map* (kbd "M-K") "exchange-direction right")
(define-key *root-map* (kbd "M-L") "exchange-direction right")
;; kill current window
(define-key *root-map* (kbd "k") "kill-window")
;; numeric window selection
(define-key *root-map* (kbd "F1") "select-window-by-number 0")
(define-key *root-map* (kbd "F2") "select-window-by-number 1")
(define-key *root-map* (kbd "F3") "select-window-by-number 2")
(define-key *root-map* (kbd "F4") "select-window-by-number 3")
(define-key *root-map* (kbd "F5") "select-window-by-number 4")
(define-key *root-map* (kbd "F6") "select-window-by-number 5")
(define-key *root-map* (kbd "F7") "select-window-by-number 6")
(define-key *root-map* (kbd "F8") "select-window-by-number 7")
(define-key *root-map* (kbd "F9") "select-window-by-number 8")
(define-key *root-map* (kbd "F10") "select-window-by-number 9")
;; move window to group
;; (define-key *root-map* (kbd "g 1") "gmove 1")
;; (define-key *root-map* (kbd "g 2") "gmove 2")
;; (define-key *root-map* (kbd "g 3") "gmove 3")
;; (define-key *root-map* (kbd "g 4") "gmove 4")
;; (define-key *root-map* (kbd "g 5") "gmove 5")
;; (define-key *root-map* (kbd "g 6") "gmove 6")
;; (define-key *root-map* (kbd "g 7") "gmove 7")
;; (define-key *root-map* (kbd "g 8") "gmove 8")
;; (define-key *root-map* (kbd "g 9") "gmove 9")
;; (define-key *root-map* (kbd "g 0") "gmove 10")
;; group selection
(undefine-key *root-map* (kbd "1"))
(define-key *root-map* (kbd "1") "gselect 1")
(undefine-key *root-map* (kbd "2"))
(define-key *root-map* (kbd "2") "gselect 2")
(undefine-key *root-map* (kbd "3"))
(define-key *root-map* (kbd "3") "gselect 3")
(undefine-key *root-map* (kbd "4"))
(define-key *root-map* (kbd "4") "gselect 4")
(undefine-key *root-map* (kbd "5"))
(define-key *root-map* (kbd "5") "gselect 5")
(undefine-key *root-map* (kbd "6"))
(define-key *root-map* (kbd "6") "gselect 6")
(undefine-key *root-map* (kbd "7"))
(define-key *root-map* (kbd "7") "gselect 7")
(undefine-key *root-map* (kbd "8"))
(define-key *root-map* (kbd "8") "gselect 8")
(undefine-key *root-map* (kbd "9"))
(define-key *root-map* (kbd "9") "gselect 9")
(undefine-key *root-map* (kbd "0"))
(define-key *root-map* (kbd "0") "gselect 10")
;; program shortcuts
(define-key *root-map* (kbd "f") "exec firefox")
(define-key *root-map* (kbd "RET") "exec urxvt")


;;;; Modules

;; TODO - should find modules automatically - try this if not
;; add module folders to load-path
;; (add-to-load-path #p"/home/matzy/.stumpwm.d/modules/util")
(load-module "notify")
(notify:notify-server-toggle)
(load-module "swm-gaps")
;; Inner gaps run along all the 4 borders of a frame
(setf swm-gaps:*inner-gaps-size* 10)
;; Outer gaps add more padding to the outermost borders
;; (touching the screen border)
(setf swm-gaps:*outer-gaps-size* 20)
;; Call command
;; toggle-gaps
(load-module "globalwindows")
(define-key *top-map* (kbd "M-1") "global-windowlist")
(define-key *top-map* (kbd "M-2") "global-pull-windowlist")
(load-module "battery-portable")
(load-module "end-session")
(load-module "undocumented")


;;;; Programs

(run-shell-command "compton")
;; (run-shell-command "polybar laptop --reload")
;; (run-shell-command "nm-applet")
;; (run-shell-command "dunst")
;; (run-shell-command "redshift")
;; (run-shell-command "gnome-encfs-manager")
;; (run-shell-command "electrum")
;; (run-shell-command "rambox")
;; (run-shell-command "megasync")
;; (run-shell-command "syncthing-gtk -m")
;; (run-shell-command "kdeconnect-indicator")
;; (run-shell-command "pulseeffects --gapplication-service")
;; (run-shell-command "patchwork")
